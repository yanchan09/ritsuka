#!/bin/sh

set -e

cargo build

qemu-system-riscv64 -machine virt \
    -serial mon:stdio \
    -kernel target/riscv64gc-unknown-none-elf/debug/ritsuka \
    -device virtio-gpu-pci,max_outputs=1 \
    -d guest_errors
