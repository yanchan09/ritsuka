use core::time::Duration;

use time::OffsetDateTime;
use volatile::Volatile;

pub struct GoldfishRtc {
    time_lo: Volatile<&'static mut u32>,
    time_hi: Volatile<&'static mut u32>,
}

impl GoldfishRtc {
    pub unsafe fn from_addr(addr: *mut u8) -> Self {
        Self {
            time_lo: Volatile::new((addr as *mut u32).as_mut().unwrap()),
            time_hi: Volatile::new((addr as *mut u32).add(1).as_mut().unwrap()),
        }
    }

    pub fn read(&mut self) -> OffsetDateTime {
        let time_lo = self.time_lo.read();
        let time_hi = self.time_hi.read();
        let time_nanos = (time_hi as u64) << 32 | time_lo as u64;
        OffsetDateTime::UNIX_EPOCH + Duration::from_nanos(time_nanos)
    }
}
