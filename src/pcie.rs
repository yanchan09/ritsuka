use core::alloc::Layout;
use core::iter;
use core::mem::forget;
use core::ptr::{copy_nonoverlapping, NonNull};
use core::ptr::{read_volatile, write_volatile};
use core::slice::from_raw_parts_mut;

use ::alloc::boxed::Box;
use ::alloc::vec::Vec;
use alloc::alloc;
use bitfield::bitfield;
use log::{debug, info};
use pci_types::device_type::DeviceType;
use pci_types::ConfigRegionAccess;
use pci_types::PciAddress;
use pci_types::PciHeader;
use riscv::asm::{delay, wfi};
use riscv::delay;
use virtio_drivers::device::gpu::VirtIOGpu;
use virtio_drivers::transport::pci::bus::{BarInfo, Cam, Command, MemoryBarType, PciRoot};
use virtio_drivers::transport::pci::{virtio_device_type, PciTransport};
use virtio_drivers::transport::{DeviceStatus, Transport};
use virtio_drivers::Hal;

#[derive(Debug, Clone, Copy)]
pub struct DevTreeRange {
    pub bus_address: DevTreeAddress,
    pub cpu_address: usize,
    pub size: usize,
}

impl DevTreeRange {
    pub fn parse_from(data: &[u8]) -> Self {
        assert!(data.len() == 28);
        let hi_bus = u32::from_be_bytes(data[0..4].try_into().unwrap());
        let mid_bus = u32::from_be_bytes(data[4..8].try_into().unwrap());
        let lo_bus = u32::from_be_bytes(data[8..12].try_into().unwrap());
        let cpu = u64::from_be_bytes(data[12..20].try_into().unwrap());
        let sz = u64::from_be_bytes(data[20..28].try_into().unwrap());
        Self {
            bus_address: DevTreeAddress(hi_bus, mid_bus, lo_bus),
            cpu_address: cpu as usize,
            size: sz as usize,
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum DevTreeSpaceCode {
    Configuration,
    IO,
    MemorySpace32Bits,
    MemorySpace64Bits,
}

#[derive(Clone, Copy)]
pub struct DevTreeAddress(pub u32, pub u32, pub u32);

impl DevTreeAddress {
    pub fn is_non_relocatable(&self) -> bool {
        self.0 >> 31 & 1 == 1
    }

    pub fn is_prefetchable(&self) -> bool {
        self.0 >> 30 & 1 == 1
    }

    pub fn is_aliased(&self) -> bool {
        self.0 >> 29 & 1 == 1
    }

    pub fn space_code(&self) -> DevTreeSpaceCode {
        match self.0 >> 24 & 0b11 {
            0b00 => DevTreeSpaceCode::Configuration,
            0b01 => DevTreeSpaceCode::IO,
            0b10 => DevTreeSpaceCode::MemorySpace32Bits,
            0b11 => DevTreeSpaceCode::MemorySpace64Bits,
            _ => unreachable!(),
        }
    }

    pub fn address_value(&self) -> usize {
        match self.space_code() {
            DevTreeSpaceCode::Configuration => (self.0 as usize & 0xffffff) << 32 | self.1 as usize,
            DevTreeSpaceCode::IO => self.2 as usize,
            DevTreeSpaceCode::MemorySpace32Bits => self.2 as usize,
            DevTreeSpaceCode::MemorySpace64Bits => (self.1 as usize) << 32 | self.2 as usize,
        }
    }
}

impl core::fmt::Debug for DevTreeAddress {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(f, "{:?}(", self.space_code())?;
        if self.is_non_relocatable() {
            write!(f, "non-relocatable, ")?;
        }
        if self.is_prefetchable() {
            write!(f, "prefetchable, ")?;
        }
        if self.is_aliased() {
            write!(f, "aliased, ")?;
        }
        write!(f, "address=0x{:x})", self.address_value())?;
        Ok(())
    }
}

pub struct SimpleRegionAllocator {
    current_pos: usize,
    total_size: usize,
}

impl SimpleRegionAllocator {
    pub fn new(size: usize) -> Self {
        Self {
            current_pos: 0,
            total_size: size,
        }
    }

    pub fn alloc_region(&mut self, size: usize) -> Option<usize> {
        // align to next `size` chunk
        let aligned_pos = (self.current_pos + size - 1) / size * size;
        if aligned_pos < self.total_size {
            self.current_pos = aligned_pos + size;
            Some(aligned_pos)
        } else {
            None
        }
    }
}

pub struct BarRegionType {
    pub space_type: DevTreeSpaceCode,
    pub aliased: bool,
    pub prefetchable: bool,
}

pub struct BarAllocator {
    regions: Vec<(BarRegionType, usize, SimpleRegionAllocator)>,
}

impl BarAllocator {
    pub fn from_dev_tree<I>(ranges: I) -> BarAllocator
    where
        I: Iterator<Item = DevTreeRange>,
    {
        Self {
            regions: ranges
                .map(|r| {
                    (
                        BarRegionType {
                            space_type: r.bus_address.space_code(),
                            aliased: r.bus_address.is_aliased(),
                            prefetchable: r.bus_address.is_aliased(),
                        },
                        r.bus_address.address_value(),
                        SimpleRegionAllocator::new(r.size),
                    )
                })
                .collect(),
        }
    }

    pub fn allocate_for(&mut self, bar: BarInfo) -> Option<usize> {
        match bar {
            BarInfo::Memory {
                address_type,
                prefetchable,
                size,
                ..
            } => {
                let base_filter: fn(&BarRegionType) -> bool = match address_type {
                    MemoryBarType::Width32 => |typ| {
                        typ.space_type == DevTreeSpaceCode::MemorySpace32Bits
                            && typ.aliased == false
                    },
                    MemoryBarType::Below1MiB => |typ| {
                        typ.space_type == DevTreeSpaceCode::MemorySpace32Bits && typ.aliased == true
                    },
                    MemoryBarType::Width64 => {
                        |typ| typ.space_type == DevTreeSpaceCode::MemorySpace64Bits
                    }
                };

                fn try_iter<'a, I>(mut iter: I, size: usize) -> Option<usize>
                where
                    I: Iterator<Item = &'a mut (BarRegionType, usize, SimpleRegionAllocator)>,
                {
                    iter.find_map(|(_, base, alloc)| {
                        alloc.alloc_region(size).map(|offset| *base + offset)
                    })
                }

                if prefetchable {
                    // if we can use prefetchable memory, try that first
                    let iter_pf = self
                        .regions
                        .iter_mut()
                        .filter(|(typ, _, _)| base_filter(&typ) && typ.prefetchable);

                    if let Some(addr) = try_iter(iter_pf, size as usize) {
                        return Some(addr);
                    }
                }

                let iter_npf = self
                    .regions
                    .iter_mut()
                    .filter(|(typ, _, _)| base_filter(&typ) && !typ.prefetchable);

                try_iter(iter_npf, size as usize)
            }
            BarInfo::IO { size, .. } => self
                .regions
                .iter_mut()
                .filter(|(typ, _, _)| typ.space_type == DevTreeSpaceCode::IO)
                .find_map(|(_, base, alloc)| {
                    alloc
                        .alloc_region(size as usize)
                        .map(|offset| *base + offset)
                }),
        }
    }
}

struct VirtioHal;

impl Hal for VirtioHal {
    fn dma_alloc(
        pages: usize,
        direction: virtio_drivers::BufferDirection,
    ) -> (virtio_drivers::PhysAddr, core::ptr::NonNull<u8>) {
        let layout = Layout::from_size_align(4096 * pages, 4096).unwrap();
        let addr = unsafe { alloc::alloc_zeroed(layout) };
        debug!("dma_alloc({}) = {:p}", pages, addr);
        (
            addr as usize - crate::kernel_vma_offset(),
            NonNull::new(addr).unwrap(),
        )
    }

    fn dma_dealloc(
        paddr: virtio_drivers::PhysAddr,
        vaddr: core::ptr::NonNull<u8>,
        pages: usize,
    ) -> i32 {
        debug!("dma_dealloc({}, {:p})", pages, vaddr);
        let layout = Layout::from_size_align(4096 * pages, 4096).unwrap();
        unsafe { alloc::dealloc(vaddr.as_ptr(), layout) }
        0
    }

    fn mmio_phys_to_virt(paddr: virtio_drivers::PhysAddr, size: usize) -> core::ptr::NonNull<u8> {
        NonNull::new((paddr + crate::kernel_vma_offset()) as *mut u8).unwrap()
    }

    fn share(
        buffer: core::ptr::NonNull<[u8]>,
        direction: virtio_drivers::BufferDirection,
    ) -> virtio_drivers::PhysAddr {
        buffer.as_ptr() as *mut u8 as usize - crate::kernel_vma_offset()
    }

    fn unshare(
        paddr: virtio_drivers::PhysAddr,
        buffer: core::ptr::NonNull<[u8]>,
        direction: virtio_drivers::BufferDirection,
    ) {
    }
}

pub fn enumerate_bus(base_addr: *mut u32) {
    let access = MmioConfigRegionAccess { base_addr };
    for bus in 0..=1 {
        for dev in 0..=31 {
            let addr = PciAddress::new(0, bus, dev, 0);
            let header = PciHeader::new(addr);
            let (vendor_id, device_id) = header.id(&access);

            if vendor_id == 0xffff {
                continue;
            }

            let (_, base_class, sub_class, _) = header.revision_and_class(&access);

            info!(
                "{} - {:04x}:{:04x} - {:?}",
                addr,
                vendor_id,
                device_id,
                DeviceType::from((base_class, sub_class))
            );

            if header.has_multiple_functions(&access) {
                for func in 1..=7 {
                    let addr = PciAddress::new(0, bus, dev, func);
                    let header = PciHeader::new(addr);
                    let (vendor_id, device_id) = header.id(&access);
                    if vendor_id == 0xffff {
                        continue;
                    }

                    info!(
                        "{} - {:04x}:{:04x} - type({})",
                        addr,
                        vendor_id,
                        device_id,
                        header.header_type(&access)
                    );
                }
            }
        }
    }
}

#[derive(Debug)]
enum PackBitsState {
    Header,
    Literal(usize),
    Repeat(u8, usize),
}

struct PackBitsIterator {
    data: &'static [u8],
    src_idx: usize,
    state: PackBitsState,
}

impl Iterator for PackBitsIterator {
    type Item = u8;

    fn next(&mut self) -> Option<Self::Item> {
        if self.src_idx >= self.data.len() {
            None
        } else {
            loop {
                //debug!("{:?}", self.state);
                match self.state {
                    PackBitsState::Header => {
                        let hbyte = self.data[self.src_idx] as i8;
                        match hbyte {
                            0..=127 => {
                                self.state = PackBitsState::Literal(hbyte as usize + 1);
                                self.src_idx += 1;
                            }
                            -127..=-1 => {
                                self.state = PackBitsState::Repeat(
                                    self.data[self.src_idx + 1],
                                    hbyte.abs() as usize + 1,
                                );
                                self.src_idx += 2;
                            }
                            -128 => {
                                self.src_idx += 1;
                            }
                        }
                    }
                    PackBitsState::Literal(mut remaining_len) => {
                        let b = self.data[self.src_idx];
                        self.src_idx += 1;

                        remaining_len -= 1;
                        if remaining_len == 0 {
                            self.state = PackBitsState::Header;
                        } else {
                            self.state = PackBitsState::Literal(remaining_len);
                        }

                        return Some(b);
                    }
                    PackBitsState::Repeat(b, mut remaining_len) => {
                        remaining_len -= 1;
                        if remaining_len == 0 {
                            self.state = PackBitsState::Header;
                        } else {
                            self.state = PackBitsState::Repeat(b, remaining_len);
                        }

                        return Some(b);
                    }
                }
            }
        }
    }
}

struct SbmaIterator {
    data: &'static [u8],
    src_idx: usize,
    state: PackBitsState,
}

impl Iterator for SbmaIterator {
    type Item = u8;

    fn next(&mut self) -> Option<Self::Item> {
        if self.src_idx >= self.data.len() {
            None
        } else {
            loop {
                match self.state {
                    PackBitsState::Header => {
                        let hbyte = self.data[self.src_idx] as i8;
                        match hbyte {
                            0..=127 => {
                                self.state = PackBitsState::Repeat(1, (hbyte as usize) + 1);
                                self.src_idx += 1;
                            }
                            -128..=-1 => {
                                self.state = PackBitsState::Repeat(0, -(hbyte as isize) as usize);
                                self.src_idx += 1;
                            }
                        }
                    }
                    PackBitsState::Literal(mut remaining_len) => {
                        let b = self.data[self.src_idx];
                        self.src_idx += 1;

                        remaining_len -= 1;
                        if remaining_len == 0 {
                            self.state = PackBitsState::Header;
                        } else {
                            self.state = PackBitsState::Literal(remaining_len);
                        }

                        return Some(b);
                    }
                    PackBitsState::Repeat(b, mut remaining_len) => {
                        remaining_len -= 1;
                        if remaining_len == 0 {
                            self.state = PackBitsState::Header;
                        } else {
                            self.state = PackBitsState::Repeat(b, remaining_len);
                        }

                        return Some(b);
                    }
                }
            }
        }
    }
}

pub fn enumerate_bus_virtio(base_addr: *mut u32, allocator: &mut BarAllocator) {
    let mut root = unsafe { PciRoot::new(base_addr as *mut u8, Cam::Ecam) };
    for (func, func_info) in root.enumerate_bus(0) {
        if let Some(dt) = virtio_device_type(&func_info) {
            if dt == virtio_drivers::transport::DeviceType::GPU {
                info!("VirtIO GPU @ {}", func);
                debug!("Mapping BARs");
                let mut was_dual = false;
                for i in 0..=5 {
                    if was_dual {
                        continue;
                    }
                    let bar_info = root.bar_info(func, i).unwrap();
                    debug!("- BAR #{}: {:?}", i, bar_info);
                    was_dual = bar_info.takes_two_entries();
                    match bar_info {
                        BarInfo::Memory {
                            address_type: MemoryBarType::Width32,
                            address,
                            size,
                            ..
                        } => {
                            if address == 0 && size > 0 {
                                let bar_start = allocator
                                    .allocate_for(bar_info)
                                    .expect("Failed to allocate BAR");
                                debug!("Setting #{} to {:x}", i, bar_start);
                                root.set_bar_32(func, i, bar_start as u32);
                            }
                        }
                        BarInfo::Memory {
                            address_type: MemoryBarType::Width64,
                            address,
                            size,
                            ..
                        } => {
                            if address == 0 && size > 0 {
                                let bar_start = allocator
                                    .allocate_for(bar_info)
                                    .expect("Failed to allocate BAR");
                                debug!("Setting #{} to {:x}", i, bar_start);
                                root.set_bar_64(func, i, bar_start as u64);
                            }
                        }
                        _ => {}
                    }
                }
                root.set_command(func, Command::MEMORY_SPACE | Command::IO_SPACE);
                debug!("Dumping new BARs");
                let mut was_dual = false;
                for i in 0..=5 {
                    if was_dual {
                        continue;
                    }
                    let bar_info = root.bar_info(func, i).unwrap();
                    debug!("- BAR #{}: {:?}", i, bar_info);
                    was_dual = bar_info.takes_two_entries();
                }
                let (status, command) = root.get_status_command(func);
                debug!("status = {:?}, command = {:?}", status, command);
                let mut transport = PciTransport::new::<VirtioHal>(&mut root, func).unwrap();
                transport.set_status(DeviceStatus::empty());
                let mut gpu = VirtIOGpu::<VirtioHal, PciTransport>::new(transport).unwrap();

                let fb = unsafe {
                    let slice = gpu.setup_framebuffer().unwrap();
                    from_raw_parts_mut(slice.as_mut_ptr(), slice.len())
                };
                //fb.copy_from_slice(include_bytes!("./gay.raw"));
                //gpu.flush().unwrap();

                /*loop {
                    unsafe {
                        wfi();
                    }
                }*/
                let mut iter = SbmaIterator {
                    data: include_bytes!("compressed.sbma2"),
                    src_idx: 0,
                    state: PackBitsState::Header,
                };
                let mut next_frame = riscv::register::time::read64();
                loop {
                    next_frame += 0x989680 / 15;
                    let stride = 1280;
                    for y in 0..360 {
                        for idx in 0..480 {
                            let dec_byte = iter.next().unwrap();
                            /*for i in 0..8 {
                                let bit_val = (dec_byte >> (7 - i)) & 1;

                                let byte_pos = stride * y + idx * 8 + i;
                                fb[byte_pos * 4..(byte_pos + 1) * 4].fill(if bit_val == 1 {
                                    255
                                } else {
                                    0
                                });
                            }*/
                            let byte_pos = stride * y * 2 + idx * 2;
                            fb[byte_pos * 4..(byte_pos + 2) * 4].fill(if dec_byte == 1 {
                                255
                            } else {
                                0
                            });
                            let byte_pos = stride * (y * 2 + 1) + idx * 2;
                            fb[byte_pos * 4..(byte_pos + 2) * 4].fill(if dec_byte == 1 {
                                255
                            } else {
                                0
                            });
                        }
                    }
                    gpu.flush().unwrap();
                    //sbi::timer::set_timer(next_frame).unwrap();
                    while riscv::register::time::read64() < next_frame {
                        unsafe {
                            riscv::asm::nop();
                        }
                    }
                }
            }
        }
    }
}

pub struct MmioConfigRegionAccess {
    base_addr: *mut u32,
}

impl MmioConfigRegionAccess {
    unsafe fn base_for_addr(&self, address: PciAddress) -> *mut u32 {
        let fn_offset = address.bus() as usize * 256
            + address.device() as usize * 8
            + address.function() as usize;
        self.base_addr.add(fn_offset * 1024)
    }
}

impl ConfigRegionAccess for MmioConfigRegionAccess {
    fn function_exists(&self, address: PciAddress) -> bool {
        todo!()
    }

    unsafe fn read(&self, address: PciAddress, offset: u16) -> u32 {
        assert!(offset % 4 == 0);
        read_volatile(self.base_for_addr(address).add(offset as usize / 4))
    }

    unsafe fn write(&self, address: PciAddress, offset: u16, value: u32) {
        assert!(offset % 4 == 0);
        write_volatile(self.base_for_addr(address).add(offset as usize / 4), value)
    }
}

unsafe impl Send for MmioConfigRegionAccess {}
