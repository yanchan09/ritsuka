#![no_std]
#![no_main]

extern crate alloc;

use core::{
    alloc::Layout,
    convert::Infallible,
    ffi::c_void,
    fmt,
    panic::PanicInfo,
    sync::atomic::{AtomicBool, AtomicU64},
};

use alloc::{
    alloc::{alloc, alloc_zeroed},
    boxed::Box,
};

use linked_list_allocator::LockedHeap;
use log::{debug, info, trace, warn};
use riscv::{
    asm::wfi,
    register::scause::{Interrupt, Trap},
};
use sbi::system_reset::{ResetReason, ResetType};
use spin::Mutex;

mod goldfish;
mod logging;
mod pcie;

extern "C" {
    static _kernelmeta: KernelMeta;
}

#[repr(C)]
struct KernelMeta {
    pub vma_offset: u64,
    pub hart_start_addr: u64,
    pub ram_reservation_start: u64,
    pub ram_reservation_end: u64,
}

fn get_kernelmeta() -> &'static KernelMeta {
    unsafe { &_kernelmeta }
}

pub fn kernel_vma_offset() -> usize {
    usize::try_from(get_kernelmeta().vma_offset).unwrap()
}

#[global_allocator]
static ALLOCATOR: LockedHeap = LockedHeap::empty();

static WRITER: Mutex<Option<Box<dyn fmt::Write + Send>>> = Mutex::new(None);

#[macro_export]
macro_rules! print {
    ($($arg:tt)*) => ($crate::_print(format_args!($($arg)*)));
}

#[macro_export]
macro_rules! println {
    () => ($crate::print!("\n"));
    ($($arg:tt)*) => ($crate::print!("{}\n", format_args!($($arg)*)));
}

#[doc(hidden)]
pub fn _print(args: fmt::Arguments) {
    if let Some(wr) = WRITER.lock().as_mut() {
        wr.write_fmt(args).unwrap();
    }
}

#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    debug!("{}", info);

    sbi::hart_state_management::hart_stop().unwrap();
    unreachable!();
}

#[repr(C)]
#[derive(Clone)]
pub struct SupervisorContext {
    pub hart_id: u64,
}

pub fn active_supervisor_context() -> SupervisorContext {
    let sscratch = riscv::register::sscratch::read();
    let ctx_ref = unsafe { (sscratch as *const SupervisorContext).as_ref().unwrap() };

    ctx_ref.clone()
}

#[no_mangle]
pub extern "C" fn kernel_handle_trap() {
    let scause = riscv::register::scause::read();
    match scause.cause() {
        Trap::Exception(ex) => {
            let ctx = active_supervisor_context();
            let pc = riscv::register::sepc::read();
            panic!(
                "[hart{}] CPU Exception ({:?}): pc = 0x{:x}",
                ctx.hart_id, ex, pc
            );
        }
        Trap::Interrupt(i) => {
            trace!("CPU interrupt: {:?}", i);
            if i == Interrupt::SupervisorTimer {
                sbi::timer::set_timer(riscv::register::time::read64() + 0x989680).unwrap();
            }
        }
    }
}

#[no_mangle]
pub extern "C" fn kernel_start_hart(hart_id: u64) -> ! {
    info!("Started hart {}", hart_id);
    unsafe {
        riscv::register::sstatus::set_sie();
        riscv::register::sie::set_stimer();
    }
    sbi::timer::set_timer(riscv::register::time::read64() + 0x989680).unwrap();
    loop {
        unsafe { wfi() };
    }
}

#[no_mangle]
pub extern "C" fn kernel_start(hart_id: u64, fdt_ptr: *mut u8) -> Infallible {
    let vma_offset = kernel_vma_offset();

    // Parse device tree
    let dt = unsafe { fdt::Fdt::from_ptr(fdt_ptr.add(vma_offset)).unwrap() };

    let timebase_freq = dt
        .find_node("/cpus")
        .expect("No /cpus node found")
        .property("timebase-frequency")
        .expect("No timebase-frequency found")
        .as_usize()
        .expect("Invalid timebase-frequency");

    // Initialize logging
    logging::init(timebase_freq as u64);

    let bootargs = dt.chosen().bootargs().unwrap_or("");

    let heap_end: usize = vma_offset + 0x80000000 + 0x8000000;
    unsafe {
        ALLOCATOR
            .lock()
            .init((heap_end - 16 * 1024 * 1024) as *mut u8, 16 * 1024 * 1024);
    }

    // Set up UART
    if let Some(node) = dt.find_compatible(&["ns16550a"]) {
        if let Some(reg) = node.reg().map(|mut r| r.next()).flatten() {
            *WRITER.lock() = Some(Box::new(unsafe {
                uart_16550::MmioSerialPort::new(reg.starting_address.add(vma_offset) as usize)
            }));
        }
    }

    println!();

    info!(
        "{}/{} starting on {}!",
        env!("CARGO_PKG_NAME"),
        env!("CARGO_PKG_VERSION"),
        env!("BUILD_TARGET")
    );

    info!("Kernel cmdline: {}", bootargs);

    let mem = dt.memory();
    trace!("Memory regions:");
    for reg in mem.regions() {
        trace!(
            "- 0x{:x}-0x{:x}",
            reg.starting_address as usize,
            reg.starting_address as usize + reg.size.unwrap_or(0)
        )
    }
    trace!("Memory reservations:");
    unsafe {
        trace!(
            "- Kernel: 0x{:x}-0x{:x}",
            _kernelmeta.ram_reservation_start,
            _kernelmeta.ram_reservation_end,
        );
    }
    trace!(
        "- Device Tree: 0x{:x}-0x{:x}",
        fdt_ptr as usize,
        fdt_ptr as usize + dt.total_size(),
    );
    for res in dt.memory_reservations() {
        trace!(
            "- 0x{:x}-0x{:x}",
            res.address() as usize,
            res.address() as usize + res.size()
        )
    }

    unsafe {
        riscv::register::sstatus::set_sie();
        riscv::register::sie::set_stimer();
    }

    trace!(
        "Enabled interrupts, sie = {}",
        riscv::register::sie::read().bits()
    );

    for cpu in dt.cpus() {
        for id in cpu.ids().all() {
            if id != hart_id as usize {
                let layout = Layout::from_size_align(16384, 4096).unwrap();
                let stack = unsafe { alloc_zeroed(layout) };
                sbi::hsm::hart_start(
                    id,
                    usize::try_from(get_kernelmeta().hart_start_addr).unwrap(),
                    stack as usize,
                )
                .unwrap();
            }
        }
    }

    sbi::timer::set_timer(riscv::register::time::read64() + 2 * (timebase_freq as u64)).unwrap();

    if let Some(node) = dt.find_compatible(&["google,goldfish-rtc"]) {
        if let Some(reg) = node.reg().map(|mut r| r.next()).flatten() {
            debug!("Goldfish RTC at {:p}", reg.starting_address);
            let mut rtc = unsafe {
                goldfish::GoldfishRtc::from_addr(reg.starting_address.add(vma_offset) as *mut u8)
            };
            info!("It's {}", rtc.read());
        }
    }

    if let Some(node) = dt.find_compatible(&["pci-host-ecam-generic"]) {
        if let Some(reg) = node.reg().map(|mut r| r.next()).flatten() {
            let offset = reg.starting_address;
            debug!("PCIe host bridge @ {:x}", offset as usize);
            pcie::enumerate_bus(unsafe { offset.add(vma_offset) } as *mut u32);

            if let Some(ranges) = node.property("ranges") {
                let range_count = ranges.value.len() / (4 * 7);
                let range_iter = (0..range_count).map(|i| {
                    let range_slice = &ranges.value[i * (4 * 7)..(i + 1) * (4 * 7)];
                    let range = pcie::DevTreeRange::parse_from(range_slice);
                    debug!("- {:?}", range);
                    assert!(!range.bus_address.is_non_relocatable(), "PCI address space with non-relocatable flag found! We don't really support that at the moment.");
                    range
                });
                let mut alloc = pcie::BarAllocator::from_dev_tree(range_iter);
                pcie::enumerate_bus_virtio(
                    unsafe { offset.add(vma_offset) as *mut u32 },
                    &mut alloc,
                );
            }
        }
    }

    debug!("Halting!");
    sbi::hart_state_management::hart_stop().unwrap()
}
