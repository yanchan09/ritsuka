INT_REGS = ['ra'] + [f'a{i}' for i in range(8)] + [f't{i}' for i in range(7)]
FP_REGS = [f'fa{i}' for i in range(8)] + [f'ft{i}' for i in range(12)]

print(".macro save_trap_context")
print("csrrw sp, sscratch, sp")
offset = 1
for reg in INT_REGS:
    print(f"sd {reg}, {offset}*8(sp)")
    offset += 1
for reg in FP_REGS:
    print(f"fsd {reg}, {offset}*8(sp)")
    offset += 1
print("csrrw t0, sscratch, sp")
print(f"sd t0, {offset}*8(sp)")
print(".endm")

print()

print(".macro restore_trap_context, base")
offset = 1
for reg in INT_REGS:
    print(f"ld {reg}, {offset}*8(sp)")
    offset += 1
for reg in FP_REGS:
    print(f"fld {reg}, {offset}*8(sp)")
    offset += 1
print(f"ld sp, {offset}*8(sp)")
print(".endm")

print()
print(f".equ SUPERVISOR_CONTEXT_SIZE, {offset*8}")
