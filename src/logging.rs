use core::sync::atomic::{AtomicU64, Ordering};

use log::Log;

struct Logger {
    start_time: AtomicU64,
    frequency: AtomicU64,
}

impl Log for Logger {
    fn enabled(&self, _: &log::Metadata) -> bool {
        true
    }

    fn log(&self, record: &log::Record) {
        let frequency = self.frequency.load(Ordering::SeqCst);
        let start_time = self.start_time.load(Ordering::SeqCst);
        let secs = if frequency != 0 {
            let time_delta = riscv::register::time::read64() - start_time;
            time_delta as f64 / frequency as f64
        } else {
            0.0
        };
        let ctx = crate::active_supervisor_context();
        crate::println!(
            "[{:.3}]{{{}}} ({}) {}",
            secs,
            ctx.hart_id,
            record.target(),
            record.args()
        );
    }

    fn flush(&self) {}
}

static LOGGER: Logger = Logger {
    start_time: AtomicU64::new(0),
    frequency: AtomicU64::new(0),
};

pub fn init(frequency: u64) {
    LOGGER
        .start_time
        .store(riscv::register::time::read64(), Ordering::SeqCst);
    LOGGER.frequency.store(frequency, Ordering::SeqCst);
    log::set_logger(&LOGGER).unwrap();
    log::set_max_level(log::LevelFilter::Trace);
}
