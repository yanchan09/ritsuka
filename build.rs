use std::env;

fn main() {
    println!(
        "cargo:rustc-env=BUILD_TARGET={}",
        env::var("TARGET").unwrap()
    );

    cc::Build::new()
        .file("src/arch/riscv/entry.S")
        .include("src/arch/riscv")
        .flag("-march=rv64gc")
        .flag("-mabi=lp64d")
        .compile("entry");

    println!("cargo:rustc-link-arg=-Tsrc/arch/riscv/linker.ld");
    println!("cargo:rerun-if-changed=src/arch/riscv/linker.ld");
    println!("cargo:rerun-if-changed=src/arch/riscv/entry.S");
}
